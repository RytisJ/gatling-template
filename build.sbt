enablePlugins(GatlingPlugin)

scalaVersion := "2.13.10"

// dependencies for Gatling
libraryDependencies ++= Seq(
  "io.gatling.highcharts" % "gatling-charts-highcharts" % "3.9.0",
  "io.gatling" % "gatling-test-framework" % "3.9.0",
  "com.iheart" %% "ficus" % "1.5.2"
)