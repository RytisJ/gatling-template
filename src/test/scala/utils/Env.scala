package utils

import com.typesafe.config.ConfigFactory
import io.gatling.core.Predef.{constantUsersPerSec, _}
import io.gatling.core.controller.inject.open.OpenInjectionStep
import io.gatling.http.Predef.http
import io.gatling.http.protocol.HttpProtocolBuilder
import net.ceedubs.ficus.Ficus._

import scala.concurrent.duration._
import scala.language.postfixOps

object Env {
  private val config = ConfigFactory.load("application.conf")

  val env: String = config.as[String]("env")

  val profile: String = config.as[String]("profile")

  val endpoint: String = config.as[String](s"$env.url")

  lazy val runtime: Int = config.as[Int]("runtime")
  lazy val tps: Int = config.as[Int]("tps")

  val simulationSettings: Seq[OpenInjectionStep] = profile match {
    case "big-bang" => Seq(constantUsersPerSec(tps).during(runtime minutes))
    case "ramp" =>
      val ramp = runtime / 3
      Seq(
        rampUsersPerSec(0).to(tps).during(ramp minutes),
        constantUsersPerSec(tps).during((runtime - ramp) minutes)
      )
    case "soak" => Seq(constantUsersPerSec(tps).during(runtime hours))
    case "smoke" => Seq(atOnceUsers(1))
    case _ => throw new Exception(s"no such profile $profile")
  }

  val httpConf: HttpProtocolBuilder = http.baseUrl(endpoint)
}
