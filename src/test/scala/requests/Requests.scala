package requests

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder

object Requests {
  def simpleGET(): HttpRequestBuilder =
    http("simple get")
    .get("/")
    .check(status.is(200))

}
