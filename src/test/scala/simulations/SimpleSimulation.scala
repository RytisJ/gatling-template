package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef.http
import io.gatling.http.protocol.HttpProtocolBuilder

class SimpleSimulation extends Simulation {

  import utils.Env._
  import requests.Requests._

  val scn: ScenarioBuilder =
    scenario("simple api test")
      .exec(simpleGET())

  setUp(scn.inject(simulationSettings).protocols(httpConf))
}
